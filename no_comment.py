#!/usr/bin/python3
from sys import argv
import re

#matching two different line starting: # or \s (any whitespace or newline)
line_starting = re.compile(r'(#|\s)')

with open(argv[1]) as file:
    while file.readline():
        line = file.readline()
        if line_starting.match(line) is None:
            print(line, end="")